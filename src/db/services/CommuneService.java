package db.services;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import bean.CommuneBean;
import bean.DistrictBean;
import db.mysql.MySQL;

public class CommuneService {
	
	public static String getNewId(){
		String code = null;
		
        try{
        	String sql = "SELECT MAX(commune_id) AS comID FROM t_commune";
        	PreparedStatement ps =MySQL.P_getConnection().prepareStatement(sql);
        	ResultSet rs = ps.executeQuery();
        	if(rs.next()){
        		String cCode = rs.getString("comID");
        		String l_code = l_cutePrefix(cCode, "com");
        		int tmpCode = Integer.parseInt(l_code);
        		tmpCode ++;
        		
        		code = "com";
        		code += String.format("%03d", tmpCode);
        	}
        	
        }catch(Exception e){
        	code = "com";
        	code += String.format("%03d", 1);
        	System.out.println("CommuneService::getNewId() => " + e.toString());
        }finally{
        	MySQL.P_getClose();
        }
		
		return code;
	}
	
	//=========================================================================
		private static String l_cutePrefix(String str, String pref){
			if(str != null && pref != null && str.startsWith(pref)){
				return str.substring(pref.length());
			}
			return str;
		}
	
	public static ArrayList<CommuneBean> p_listAllCommuneByDistrictID(String id){
		ArrayList<CommuneBean> al = new ArrayList<CommuneBean>();
		try{
			String sql = "SELECT * FROM t_commune WHERE district_id = ?";
			PreparedStatement ps = MySQL.P_getConnection().prepareStatement(sql);
			ps.setString(1, id);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()){
				CommuneBean cb = new CommuneBean();
				cb.setCommune_id(rs.getString("commune_id"));
				cb.setCommune(rs.getString("commune"));
				cb.setDistrict_id(rs.getString("district_id"));
				
				
				al.add(cb);
			}
		}catch(Exception e){
			System.out.println(" show error!" + e.toString());
		}finally{
			MySQL.P_getClose();
		}
		return al;
	}
	
	public static ArrayList<CommuneBean> p_listAllCommune(){
		ArrayList<CommuneBean> al = new ArrayList<CommuneBean>();
		try{
			String sql = "SELECT * FROM t_commune";
			PreparedStatement ps = MySQL.P_getConnection().prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()){
				CommuneBean cb = new CommuneBean();
				cb.setCommune_id(rs.getString("commune_id"));
				cb.setCommune(rs.getString("commune"));
				cb.setDistrict_id(rs.getString("district_id"));
				
				al.add(cb);
			}
		}catch(Exception e){
			System.out.println(" show error!" + e.toString());
		}finally{
			MySQL.P_getClose();
		}
		return al;
	}
	
	public static String addNewCommune(CommuneBean cb){
		String message = "";
		try{
        	String sql = "INSERT INTO t_commune VALUES(?,?,?)";
        	PreparedStatement ps = MySQL.P_getConnection().prepareStatement(sql);
        	ps.setString(1, cb.getCommune_id());
        	ps.setString(2, cb.getCommune());
        	
        	DistrictBean db = cb.getDb();
        	ps.setString(3, db.getDistrict_id());
			
        	int state = ps.executeUpdate();
        	if(state > 0 ){		
				message = "success";
			}
		}catch(Exception e){
			message = "error";
        	System.out.println("CommuneService::addNewCommune() => " + e.toString());
        }finally{
        	MySQL.P_getClose();;
        }
		return message;
	}
}
