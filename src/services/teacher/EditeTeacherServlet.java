package services.teacher;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Pattern;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import bean.AddressBean;
import bean.TeacherBean;

@WebServlet("/EditeTeacherServlet")
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 10,maxFileSize = 1024 * 1024 * 50,maxRequestSize = 1024 * 1024 * 100)

public class EditeTeacherServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public EditeTeacherServlet() {
        super();
    }
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
	      response.setContentType("text/html; charset=UTF-8");
	      
	      String teacher_id = request.getParameter("teacher_id");
			String first_name = request.getParameter("first_name");
			String last_name = request.getParameter("last_name");
			String gender = request.getParameter("gender");
			String nationality =request.getParameter("nationality");
			String nationality_id =request.getParameter("nationality_id");
			String dob =request.getParameter("dob");
			String village_id= request.getParameter("village_id");
			String passport_no =request.getParameter("passport_no");
			String phone =request.getParameter("phone_number");
			String country =request.getParameter("country");
			String status =request.getParameter("status");
			String current_village = request.getParameter("current_village");
			
			Part part = request.getPart("file");
			String fileName = fileNameFilter(part);
			
				    
	    TeacherBean teb = new TeacherBean();
	    AddressBean ab =new AddressBean();
	    AddressBean ad =new AddressBean();
	    if(!fileName.equals("")){ // have new photo to update
	      String savePath = "\\images\\teachers\\";
	      Path path = Paths.get("images/teachers");
	      String urlPath= "D:\\JavaProjects\\LMS\\WebContent\\images\\teachers\\" + File.separator + fileName;
	      System.out.println("url = " + urlPath);
	      File fileSaveDirectory = new File(urlPath);
	      part.write(urlPath + File.separator);
	      
	      //to deletáŸ� old photo
	      ServletContext context = getServletContext();
	      String oldFileName = request.getParameter("old_photo");
	      deleteExistImage(context,oldFileName);

			teb.setT_id(teacher_id);
		   	teb.setT_fname(first_name);
		   	teb.setT_lname(last_name);
			teb.setT_gender(gender);
		    ab.setVillage_id(village_id);
		   	teb.setaddress_teacher(ab);
		   	teb.setT_dob(dob);
		   	teb.setT_phone(phone);
		   	teb.setNationality(nationality);
		    teb.setCountry(country);
		    teb.setPassport_no(passport_no);
		    teb.setNationality_id(nationality_id);
		    ad.setVillage_id(current_village);
		    teb.setCurrent_address(ad);
		    teb.setStustus(status);
		    teb.setT_photo(fileName);
		    teb.setPhoto_url(savePath);
	    }else{
	    	
	    	teb.setT_id(teacher_id);
		   	teb.setT_fname(first_name);
		   	teb.setT_lname(last_name);
			teb.setT_gender(gender);
			ab.setVillage_id(village_id);
		   	teb.setaddress_teacher(ab);
		   	teb.setT_dob(dob);
		   	teb.setT_phone(phone);
		   	teb.setNationality(nationality);
		   	teb.setCountry(country);
		    teb.setPassport_no(passport_no);
		    ad.setVillage_id(current_village);
		    teb.setCurrent_address(ad);
		    teb.setNationality_id(nationality_id);
		    teb.setStustus(status);
		    teb.setT_photo("");
	    }
	    
	    String msg = db.services.TeacherService.UpdateATeacher(teb);
	    HttpSession session = request.getSession(false);
	    session.setAttribute("code", msg);
	    response.sendRedirect("./view/update/UpdateATeacher?id=" + teb.getT_id());
	    
	  }
	  
	  private void deleteExistImage(ServletContext context, String OldImageName){
	    String urlPath = "D:\\JavaProjects\\LMS\\WebContent\\images\\teachers\\" + File.separator + OldImageName;
	    new File(urlPath).delete();
	    
	  }
	  
	  public static String fileNameFilter(Part part){
	    String contentDisplay = part.getHeader("content-disposition");
	    
	    System.out.println("contentDisplay = " + contentDisplay);
	    String []items = contentDisplay.split(";");
	    for(String str : items){
	      if(str.trim().startsWith("filename")){
	        String name = str.substring(str.indexOf("=") + 2, str.length() -1 );
	        System.out.println("fname = " + name);
	        String []fname = name.split( Pattern.quote(File.separator) );
	        return fname[fname.length - 1];
	        //return str.substring(str.indexOf("=") + 2, str.length()-1);
	      }
	    }
	    return "";
	  }
		
		
}

