package bean;

public class ProvinceBean {
	private String pro_id,province,counttry_id;
	public ProvinceBean(){};
	public ProvinceBean(String id, String proName,String countid){
		this.pro_id = id;
		this.province = proName;
		this.counttry_id = countid;
	}
	public String getPro_id() {
		return pro_id;
	}
	public void setPro_id(String pro_id) {
		this.pro_id = pro_id;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCounttry_id() {
		return counttry_id;
	}
	public void setCounttry_id(String counttry_id) {
		this.counttry_id = counttry_id;
	}
	
	
}
