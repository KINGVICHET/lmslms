package bean;

public class VillageBean {
	private String village_id,village,commune_id;
	private CommuneBean cb;	

	
	public CommuneBean getCb() {
		return cb;
	}
	public void setCb(CommuneBean cb) {
		this.cb = cb;
	}
	public VillageBean(){}
	public VillageBean(String villid,String village,String comid){
		this.village_id = villid;
		this.village = village;
		this.commune_id = comid;
	}
	public String getVillage_id() {
		return village_id;
	}
	public void setVillage_id(String village_id) {
		this.village_id = village_id;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getCommune_id() {
		return commune_id;
	}
	public void setCommune_id(String commune_id) {
		this.commune_id = commune_id;
	}
	
	
}
