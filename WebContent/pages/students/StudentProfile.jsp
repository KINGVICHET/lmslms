<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>  
<%@ page import="java.util.ArrayList" %>
<%@ page import="db.services.StudentService" %>
<%@ page import="bean.StudentBean" %>   
<%
	session = request.getSession(false);
	String role_code = "",role_name="",user_name="";
	String photo_view = "";
	if(session != null){
		if(session.getAttribute("user_name")!= null){
			user_name = session.getAttribute("user_name").toString();
			photo_view = "." + session.getAttribute("photo_url").toString() + "/" + session.getAttribute("photo_name").toString();
					
			role_code = session.getAttribute("role_code").toString();
			role_name = session.getAttribute("role_name").toString();
		}else{
			response.sendRedirect("../../AccessSystem");
		}
	}else{
		//redirect to login page
		response.sendRedirect("../../AccessSystem");
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="images/favicon.ico" type="image/ico" />
    <title>LMS</title>  
<!-- Bootstrap -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../../css/font-awesome.min.css" rel="stylesheet">
    <link href="../../webcss/all.min.css" rel="stylesheet"> <!--load all styles -->
    <link href="../../webcss/fontawesome.min.css" rel="stylesheet"> 
    <!-- Custom Theme Style -->
    <link href="../../css/custom.min.css" rel="stylesheet">
     <!-- jQuery -->   
    <link href="../../css/dataTable/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../../css/dataTable/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../../css/dataTable/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../../css/dataTable/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../../css/dataTable/scroller.bootstrap.min.css" rel="stylesheet">
    <script src="../../js/jquery.min.js"></script> 
    <script src="../../js/custom_js/lock_screen.js"></script>
  </head>
<body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="#" class="site_title"><i class="fas fa-school fa-2x"></i> <span>School<b>MS</b></span></a>
            </div>
            <!-- sidebar menu -->
			<jsp:include page="/view/detail/LeftMenu">
				<jsp:param value="<%=user_name %>" name="user_name"/>
				<jsp:param value="<%=photo_view %>" name="photo_view"/>
				<jsp:param value="<%=role_name %>" name="role_name"/>
				<jsp:param value="<%=role_code %>" name="role_code"/>
			</jsp:include>
            <!-- /sidebar menu -->
          </div>
        </div>
        <!-- top navigation -->
       	<jsp:include page="/view/detail/Banner">
       		<jsp:param value="<%=user_name %>" name="user_name"/>
       		<jsp:param value="<%=photo_view %>" name="photo_view"/>
       		<jsp:param value="<%=role_name %>" name="role_name"/>
       	</jsp:include>
        <!-- page content -->
	     <div class="right_col" role="main">
	        <div class="col-md-12 col-sm-12 ">
	        	<div class="x_panel">
	            	<div class="x_title">
	                	<h2><i class="fas fa-user-graduate fa-xl"></i>  Student Profile</h2>
	                    <ul class="nav navbar-right panel_toolbox">
	                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
	                      <li class="dropdown">
	                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
	                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
	                            <a class="dropdown-item" href="#">Settings 1</a>
	                            <a class="dropdown-item" href="#">Settings 2</a>
	                        </div>
	                      </li>
	                      <li><a class="close-link"><i class="fa fa-close"></i></a></li>
	                    </ul>
	                    <div class="clearfix"></div>
	                 </div>
	                 <div class="col-md-4">
                        <aside class="profile-nav alt">
                            <section class="card">
                                <div class="card-header user-header alt bg-dark">
                                    <div class="media">
                                        <a href="#">
                                            <img class="align-self-center rounded-circle mr-3" style="width:85px; height:85px;" alt="" src="../../images/Avatar/admin.jpg">
                                        </a>
                                        <div class="media-body">
                                            <h2 class="text-light display-6">Roeurn Chamroeun</h2>
                                            <p class="text-white">Computer Science</p>
                                        </div>
                                    </div>
                                </div>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">
                                        <a href="#"> <i class="fa fa-map-marker"></i> Moung Russey,Battambang <span class="badge badge-primary pull-right">10</span></a>
                                    </li>
                                    <li class="list-group-item">
                                        <a href="#"> <i class="fa fa-tasks"></i>  <span class="badge badge-danger pull-right">15</span></a>
                                    </li>
                                    <li class="list-group-item">
                                        <a href="#"> <i class="fa fa-bell-o"></i> Notification <span class="badge badge-success pull-right">11</span></a>
                                    </li>
                                    <li class="list-group-item">
                                        <a href="#"> <i class="fa fa-comments-o"></i> Message <span class="badge badge-warning pull-right r-activity">03</span></a>
                                    </li>
                                </ul>

                            </section>
                        </aside>
                    </div>
                    <div class="col-md-8 col-sm-8">
	                    <div class="x_content">
		                	<ul class="nav nav-tabs bar_tabs" id="myTab" role="tablist">
		                    	<li class="nav-item">
		                        	<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"><i class="fas fa-user-graduate fa-lg"></i> Detail Personal Information</a>
		                      	</li>
		                      	<li class="nav-item">
		                        	<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false"><i class="fa fa-pencil-square-o"></i> Change Student Class</a>
		                      	</li>
		                    </ul>
		                    <div class="tab-content" id="myTabContent">
			                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
			                      	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			                            <div class="review-tab-pro-inner">
			                                <ul id="myTab3" class="tab-review-design">
			                                    <li class="active"><a href="#description"><i class="icon nalika-edit" aria-hidden="true"></i> Product Edit</a></li>
			                                    <li class=""><a href="#reviews"><i class="icon nalika-picture" aria-hidden="true"></i> Pictures</a></li>
			                                    <li class=""><a href="#INFORMATION"><i class="icon nalika-chat" aria-hidden="true"></i> Review</a></li>
			                                </ul>
			                                <div id="myTabContent" class="tab-content custom-product-edit">
			                                    <div class="product-tab-list tab-pane fade active in" id="description">
			                                        <div class="row">
			                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			                                                <div class="review-content-section">
			                                                    <div class="input-group mg-b-pro-edt">
			                                                        <span class="input-group-addon"><i class="icon nalika-user" aria-hidden="true"></i></span>
			                                                        <input type="text" class="form-control" placeholder="First Name">
			                                                    </div>
			                                                    <div class="input-group mg-b-pro-edt">
			                                                        <span class="input-group-addon"><i class="icon nalika-edit" aria-hidden="true"></i></span>
			                                                        <input type="text" class="form-control" placeholder="Product Title">
			                                                    </div>
			                                                    <div class="input-group mg-b-pro-edt">
			                                                        <span class="input-group-addon"><i class="fa fa-usd" aria-hidden="true"></i></span>
			                                                        <input type="text" class="form-control" placeholder="Regular Price">
			                                                    </div>
			                                                    <div class="input-group mg-b-pro-edt">
			                                                        <span class="input-group-addon"><i class="icon nalika-new-file" aria-hidden="true"></i></span>
			                                                        <input type="text" class="form-control" placeholder="Tax">
			                                                    </div>
			                                                    <div class="input-group mg-b-pro-edt">
			                                                        <span class="input-group-addon"><i class="icon nalika-favorites" aria-hidden="true"></i></span>
			                                                        <input type="text" class="form-control" placeholder="Quantity">
			                                                    </div>
			                                                </div>
			                                            </div>
			                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			                                                <div class="review-content-section">
			                                                    <div class="input-group mg-b-pro-edt">
			                                                        <span class="input-group-addon"><i class="icon nalika-user" aria-hidden="true"></i></span>
			                                                        <input type="text" class="form-control" placeholder="Last Name">
			                                                    </div>
			                                                    <div class="input-group mg-b-pro-edt">
			                                                        <span class="input-group-addon"><i class="icon nalika-favorites-button" aria-hidden="true"></i></span>
			                                                        <input type="text" class="form-control" placeholder="Product Description">
			                                                    </div>
			                                                    <div class="input-group mg-b-pro-edt">
			                                                        <span class="input-group-addon"><i class="fa fa-usd" aria-hidden="true"></i></span>
			                                                        <input type="text" class="form-control" placeholder="Sale Price">
			                                                    </div>
			                                                    <div class="input-group mg-b-pro-edt">
			                                                        <span class="input-group-addon"><i class="icon nalika-like" aria-hidden="true"></i></span>
			                                                        <input type="text" class="form-control" placeholder="Category">
			                                                    </div>
			                                                    <select name="select" class="form-control pro-edt-select form-control-primary">
																		<option value="opt1">Select One Value Only</option>
																		<option value="opt2">2</option>
																		<option value="opt3">3</option>
																		<option value="opt4">4</option>
																		<option value="opt5">5</option>
																		<option value="opt6">6</option>
																	</select>
			                                                </div>
			                                            </div>
			                                        </div>
			                                        <div class="row">
			                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			                                                <div class="text-center custom-pro-edt-ds">
			                                                    <button type="button" class="btn btn-ctl-bt waves-effect waves-light m-r-10">Save
																	</button>
			                                                    <button type="button" class="btn btn-ctl-bt waves-effect waves-light">Discard
																	</button>
			                                                </div>
			                                            </div>
			                                        </div>
			                                    </div>
			                                    <div class="product-tab-list tab-pane fade" id="reviews">
			                                        <div class="row">
			                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			                                                <div class="review-content-section">
			                                                    <div class="row">
			                                                        <div class="col-lg-4">
			                                                            <div class="pro-edt-img">
			                                                                <img src="img/new-product/5-small.jpg" alt="">
			                                                            </div>
			                                                        </div>
			                                                        <div class="col-lg-8">
			                                                            <div class="row">
			                                                                <div class="col-lg-12">
			                                                                    <div class="product-edt-pix-wrap">
			                                                                        <div class="input-group">
			                                                                            <span class="input-group-addon">TT</span>
			                                                                            <input type="text" class="form-control" placeholder="Label Name">
			                                                                        </div>
			                                                                        <div class="row">
			                                                                            <div class="col-lg-6">
			                                                                                <div class="form-radio">
			                                                                                    <form>
			                                                                                        <div class="radio radiofill">
			                                                                                            <label>
																												<input type="radio" name="radio"><i class="helper"></i>Largest Image
																											</label>
			                                                                                        </div>
			                                                                                        <div class="radio radiofill">
			                                                                                            <label>
																												<input type="radio" name="radio"><i class="helper"></i>Medium Image
																											</label>
			                                                                                        </div>
			                                                                                        <div class="radio radiofill">
			                                                                                            <label>
																												<input type="radio" name="radio"><i class="helper"></i>Small Image
																											</label>
			                                                                                        </div>
			                                                                                    </form>
			                                                                                </div>
			                                                                            </div>
			                                                                            <div class="col-lg-6">
			                                                                                <div class="product-edt-remove">
			                                                                                    <button type="button" class="btn btn-ctl-bt waves-effect waves-light">Remove
																										<i class="fa fa-times" aria-hidden="true"></i>
																									</button>
			                                                                                </div>
			                                                                            </div>
			                                                                        </div>
			                                                                    </div>
			                                                                </div>
			                                                            </div>
			                                                        </div>
			                                                    </div>
			                                                    <div class="row">
			                                                        <div class="col-lg-4">
			                                                            <div class="pro-edt-img">
			                                                                <img src="img/new-product/6-small.jpg" alt="">
			                                                            </div>
			                                                        </div>
			                                                        <div class="col-lg-8">
			                                                            <div class="row">
			                                                                <div class="col-lg-12">
			                                                                    <div class="product-edt-pix-wrap">
			                                                                        <div class="input-group">
			                                                                            <span class="input-group-addon">TT</span>
			                                                                            <input type="text" class="form-control" placeholder="Label Name">
			                                                                        </div>
			                                                                        <div class="row">
			                                                                            <div class="col-lg-6">
			                                                                                <div class="form-radio">
			                                                                                    <form>
			                                                                                        <div class="radio radiofill">
			                                                                                            <label>
																												<input type="radio" name="radio"><i class="helper"></i>Largest Image
																											</label>
			                                                                                        </div>
			                                                                                        <div class="radio radiofill">
			                                                                                            <label>
																												<input type="radio" name="radio"><i class="helper"></i>Medium Image
																											</label>
			                                                                                        </div>
			                                                                                        <div class="radio radiofill">
			                                                                                            <label>
																												<input type="radio" name="radio"><i class="helper"></i>Small Image
																											</label>
			                                                                                        </div>
			                                                                                    </form>
			                                                                                </div>
			                                                                            </div>
			                                                                            <div class="col-lg-6">
			                                                                                <div class="product-edt-remove">
			                                                                                    <button type="button" class="btn btn-ctl-bt waves-effect waves-light">Remove
																										<i class="fa fa-times" aria-hidden="true"></i>
																									</button>
			                                                                                </div>
			                                                                            </div>
			                                                                        </div>
			                                                                    </div>
			                                                                </div>
			                                                            </div>
			                                                        </div>
			                                                    </div>
			                                                    <div class="row">
			                                                        <div class="col-lg-4">
			                                                            <div class="pro-edt-img mg-b-0">
			                                                                <img src="img/new-product/7-small.jpg" alt="">
			                                                            </div>
			                                                        </div>
			                                                        <div class="col-lg-8">
			                                                            <div class="row">
			                                                                <div class="col-lg-12">
			                                                                    <div class="product-edt-pix-wrap">
			                                                                        <div class="input-group">
			                                                                            <span class="input-group-addon">TT</span>
			                                                                            <input type="text" class="form-control" placeholder="Label Name">
			                                                                        </div>
			                                                                        <div class="row">
			                                                                            <div class="col-lg-6">
			                                                                                <div class="form-radio">
			                                                                                    <form>
			                                                                                        <div class="radio radiofill">
			                                                                                            <label>
																												<input type="radio" name="radio"><i class="helper"></i>Largest Image
																											</label>
			                                                                                        </div>
			                                                                                        <div class="radio radiofill">
			                                                                                            <label>
																												<input type="radio" name="radio"><i class="helper"></i>Medium Image
																											</label>
			                                                                                        </div>
			                                                                                        <div class="radio radiofill">
			                                                                                            <label>
																												<input type="radio" name="radio"><i class="helper"></i>Small Image
																											</label>
			                                                                                        </div>
			                                                                                    </form>
			                                                                                </div>
			                                                                            </div>
			                                                                            <div class="col-lg-6">
			                                                                                <div class="product-edt-remove">
			                                                                                    <button type="button" class="btn btn-ctl-bt waves-effect waves-light">Remove
																										<i class="fa fa-times" aria-hidden="true"></i>
																									</button>
			                                                                                </div>
			                                                                            </div>
			                                                                        </div>
			                                                                    </div>
			                                                                </div>
			                                                            </div>
			                                                        </div>
			                                                    </div>
			                                                </div>
			                                            </div>
			                                        </div>
			                                    </div>
			                                    <div class="product-tab-list tab-pane fade" id="INFORMATION">
			                                        <div class="row">
			                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			                                                <div class="review-content-section">
			                                                    <div class="card-block">
			                                                        <div class="text-muted f-w-400">
			                                                            <p>No reviews yet.</p>
			                                                        </div>
			                                                        <div class="m-t-10">
			                                                            <div class="txt-primary f-18 f-w-600">
			                                                                <p>Your Rating</p>
			                                                            </div>
			                                                            <div class="stars stars-example-css detail-stars">
			                                                                <div class="review-rating">
			                                                                    <fieldset class="rating">
			                                                                        <input type="radio" id="star5" name="rating" value="5">
			                                                                        <label class="full" for="star5"></label>
			                                                                        <input type="radio" id="star4half" name="rating" value="4 and a half">
			                                                                        <label class="half" for="star4half"></label>
			                                                                        <input type="radio" id="star4" name="rating" value="4">
			                                                                        <label class="full" for="star4"></label>
			                                                                        <input type="radio" id="star3half" name="rating" value="3 and a half">
			                                                                        <label class="half" for="star3half"></label>
			                                                                        <input type="radio" id="star3" name="rating" value="3">
			                                                                        <label class="full" for="star3"></label>
			                                                                        <input type="radio" id="star2half" name="rating" value="2 and a half">
			                                                                        <label class="half" for="star2half"></label>
			                                                                        <input type="radio" id="star2" name="rating" value="2">
			                                                                        <label class="full" for="star2"></label>
			                                                                        <input type="radio" id="star1half" name="rating" value="1 and a half">
			                                                                        <label class="half" for="star1half"></label>
			                                                                        <input type="radio" id="star1" name="rating" value="1">
			                                                                        <label class="full" for="star1"></label>
			                                                                        <input type="radio" id="starhalf" name="rating" value="half">
			                                                                        <label class="half" for="starhalf"></label>
			                                                                    </fieldset>
			                                                                </div>
			                                                                <div class="clear"></div>
			                                                            </div>
			                                                        </div>
			                                                        <div class="input-group mg-b-15 mg-t-15">
			                                                            <span class="input-group-addon"><i class="icon nalika-user" aria-hidden="true"></i></span>
			                                                            <input type="text" class="form-control" placeholder="User Name">
			                                                        </div>
			                                                        <div class="input-group mg-b-15">
			                                                            <span class="input-group-addon"><i class="icon nalika-user" aria-hidden="true"></i></span>
			                                                            <input type="text" class="form-control" placeholder="Last Name">
			                                                        </div>
			                                                        <div class="input-group mg-b-15">
			                                                            <span class="input-group-addon"><i class="icon nalika-mail" aria-hidden="true"></i></span>
			                                                            <input type="text" class="form-control" placeholder="Email">
			                                                        </div>
			                                                        <div class="form-group review-pro-edt mg-b-0-pt">
			                                                            <button type="submit" class="btn btn-ctl-bt waves-effect waves-light">Submit
																			</button>
			                                                        </div>
			                                                    </div>
			                                                </div>
			                                            </div>
			                                        </div>
			                                    </div>
			                                </div>
			                            </div>
			                        </div>
					        		
			                    </div>
			                      
			                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
			                        <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle" src="../../dist/img/user4-128x128.jpg" alt="User profile picture">
                </div>

                <h3 class="profile-username text-center">Nina Mcintire</h3>

                <p class="text-muted text-center">Software Engineer</p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Followers</b> <a class="float-right">1,322</a>
                  </li>
                  <li class="list-group-item">
                    <b>Following</b> <a class="float-right">543</a>
                  </li>
                  <li class="list-group-item">
                    <b>Friends</b> <a class="float-right">13,287</a>
                  </li>
                </ul>

                <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>
              </div>
              <!-- /.card-body -->
            </div>
			                    </div>
		              		</div>
	                	</div>
                	</div>
	         	</div>   
	       	</div>  
	     </div>
        <!-- /page content -->
        <!-- footer content -->
		<jsp:include page="/view/detail/FooterPage"></jsp:include>
        <!-- /footer content -->
      </div>
    </div>
    <!-- Bootstrap -->
    <script src="../../js/bootstrap.bundle.min.js"></script>
    <script src='../../js/custom_js/bootbox.min.js'></script>
    <!-- Datatables -->
    <script src="../../js/dataTable/jquery.dataTables.min.js"></script>
    <script src="../../js/dataTable/dataTables.bootstrap.min.js"></script>
    <script src="../../js/dataTable/dataTables.responsive.min.js"></script>
    <script src="../../js/dataTable/responsive.bootstrap.js"></script>
    <script src="../../js/dataTable/buttons.bootstrap.min.js"></script>
    <script src="../../js/dataTable/dataTables.scroller.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="../../js/custom.min.js"></script>

    <script>
		$(document).ready(function(){
		  $('[data-toggle="tooltip"]').tooltip();   
		});
	</script>
</body>
</html>