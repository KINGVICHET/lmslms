<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList" %>
<%@ page import="bean.ProvinceBean" %>
<%@ page import="bean.CommuneBean" %>
<%@ page import="bean.CountryBean" %>
<%@ page import="bean.DistrictBean" %>
<%@ page import="bean.VillageBean" %>
<%@ page import="bean.TeacherBean" %>
<%@ page import="db.services.ProvinceService" %>
<%@ page import="db.services.TeacherService" %>
    
<%
	session = request.getSession(false);
	String role_code = "",role_name="",user_name="";
	String photo_view = "";
	if(session != null){
		
		if(session.getAttribute("user_name")!= null){
			user_name = session.getAttribute("user_name").toString();
			photo_view = "." + session.getAttribute("photo_url").toString() + "/" + session.getAttribute("photo_name").toString();
					
			role_code = session.getAttribute("role_code").toString();
			role_name = session.getAttribute("role_name").toString();
			
		}else{
			response.sendRedirect("../../AccessSystem");
		}
	}else{
		//redirect to login page
		response.sendRedirect("../../AccessSystem");
	}
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>LMS</title>

    <link href="../../css/bootstrap.min.css" rel="stylesheet">
    <link href="../../css/font-awesome.min.css" rel="stylesheet">
    <link href="../../webcss/all.min.css" rel="stylesheet"> 
    <link href="../../css/custom.min.css" rel="stylesheet">
     <link href="../../css/customStyle/MaltyFormData.css" rel="stylesheet">
    <script src="../../js/jquery.min.js"></script>   
	
    
    <script>
           
    $(document).ready(function(){
		$("#province").on('change', function()
		{
			var params = {proid : $(this).val()};
				$.post("${pageContext.request.contextPath}/ProvinceServlet" , $.param(params) , function(responseJson)
						{
							var $select = $("#district");
							$select.find("option").remove();
							$.each(responseJson, function(idx, key)
							{
								console.log( key );
								$("<option>").val( key.district_id ).text(key.district).appendTo($select);
					        });
				        });
		});
	});
	
	

	$(document).ready(function(){
		$("#district").on('change', function()
		{
			var params = {distid : $(this).val()};
				$.post("${pageContext.request.contextPath}/SelectCommuneServlet" , $.param(params) , function(responseJson)
				{
					var $select = $("#commune");
					$select.find("option").remove();
					$.each(responseJson, function(idx, key)
					{
						console.log( key );
						$("<option>").val( key.commune_id ).text(key.commune).appendTo($select);
					});
				});
		});
	});
		
	

	$(document).ready(function(){
		$("#commune").on('change', function()
		{
			
			var params = {comid : $(this).val()};				
				$.post("${pageContext.request.contextPath}/SelectVillageServlet" , $.param(params) , function(responseJson)
				{
					var $select = $("#village");
					$select.find("option").remove();
					$.each(responseJson, function(idx, key)
					{
						console.log( key );
						$("<option>").val( key.village_id ).text(key.village).appendTo($select);
					});
				});
		});
	});
		
	
	$(document).ready(function(){
		$("#current_province").on('change', function()
		{
			var params = {proid : $(this).val()};
				$.post("${pageContext.request.contextPath}/ProvinceServlet" , $.param(params) , function(responseJson)
						{
							var $select = $("#current_district");
							$select.find("option").remove();
							$.each(responseJson, function(idx, key)
							{
								console.log( key );
								$("<option>").val( key.district_id ).text(key.district).appendTo($select);
					        });
				        });
		});
	});
	
	

	$(document).ready(function(){
		$("#current_district").on('change', function()
		{
			var params = {distid : $(this).val()};
				$.post("${pageContext.request.contextPath}/SelectCommuneServlet" , $.param(params) , function(responseJson)
				{
					var $select = $("#current_commune");
					$select.find("option").remove();
					$.each(responseJson, function(idx, key)
					{
						console.log( key );
						$("<option>").val( key.commune_id ).text(key.commune).appendTo($select);
					});
				});
		});
	});
		
	

	$(document).ready(function(){
		$("#current_commune").on('change', function()
		{
			
			var params = {comid : $(this).val()};				
				$.post("${pageContext.request.contextPath}/SelectVillageServlet" , $.param(params) , function(responseJson)
				{
					var $select = $("#current_village");
					$select.find("option").remove();
					$.each(responseJson, function(idx, key)
					{
						console.log( key );
						$("<option>").val( key.village_id ).text(key.village).appendTo($select);
					});
				});
		});
	});
	</script>
  	
	
    		
  </head>
<body class="nav-md">

    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="#" class="site_title"><i class="fas fa-school fa-2x"></i> <span>School<b>MS</b></span></a>
            </div>

            <!-- sidebar menu -->
			<jsp:include page="/view/detail/LeftMenu">
				<jsp:param value="<%=user_name %>" name="user_name"/>
				<jsp:param value="<%=photo_view %>" name="photo_view"/>
				<jsp:param value="<%=role_name %>" name="role_name"/>
				<jsp:param value="<%=role_code %>" name="role_code"/>
			</jsp:include>
            <!-- /sidebar menu -->
          </div>
        </div>

        <!-- top navigation -->
       	<jsp:include page="/view/detail/Banner">
       		<jsp:param value="<%=user_name %>" name="user_name"/>
       		<jsp:param value="<%=photo_view %>" name="photo_view"/>
       		<jsp:param value="<%=role_name %>" name="role_name"/>
       	</jsp:include>

        <!-- page content -->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2 id="title_student">		Teacher Information</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
						
                  </div>
   <form role="form" class="registration-form" action="${pageContext.request.contextPath}/AddANewTeacherServlet" Method="POST"  enctype="multipart/form-data" >
                    
             <fieldset id ="step_one">
             
                   <div class="form-top">
                       <div class="form-top-left">
                          <h3><span><i class="fa fa-calendar-check-o" aria-hidden="true"></i></span>		Teacher Information</h3>
                               
                        </div>
                    </div>
                    
              <div class="form-bottom">
              <div class="row">
                                 <%
	                        		String teacher_id = TeacherService.getteacherId();
                                 
	                        	 %>
                                
                  <div class="col-md-9 col-sm-12" id="bordy_top" >
	                      <div class="form-group row" >
	                         <label class="control-label col-md-4 col-sm-3 col-xs-3">Teacher ID :</label>
	                            <div class="col-md-8 col-sm-9 col-xs-9">
	                             <input type="text" name="teacher_id" id="teacher_id" value="<%=teacher_id %>" class="form-control" disabled>
	                              <input type="hidden" name="teacher_id" id="teacher_id" value="<%=teacher_id %>" class="form-control">
	                           </div>
	                      </div>
	                      
	                      <div class="form-group row" id="bordy_first_name">
	                         <label class="control-label col-md-4 col-sm-3 col-xs-3">First Name :</label>
	                           <div class="col-md-8 col-sm-9 col-xs-9">
	                            <small class="text-danger bold italic" id="fname_error"></small>
	                           <input type="text" class="form-control" id="first_name" name = "first_name" placeholder= "Teacher_First_Name" autocomplete="off" >
	                         </div>
	                      </div>
	                      
	                     <div class="form-group row" id="bordy_last_name" >
	                        <label class="control-label col-md-4 col-sm-3 col-xs-3">Last Name :</label>
	                            <div class="col-md-8 col-sm-9 col-xs-9">
	                            <small class="text-danger bold italic" id="lname_error"></small>
	                            <input type="text" class="form-control" id="last_name" name="last_name" placeholder ="Teacher_Last_Name" autocomplete="off" >
	                        </div>
	                     </div>
	                      
	                      <div class="form-group row" id="bordy_gender">
	                          <label class="control-label col-md-4 col-sm-3 col-xs-3">Gender :</label>
	                              <div class="col-md-8 col-sm-9 col-xs-9 input-error" >
	                                  <small class="text-danger bold italic" id="gender_error"></small>
	                                     <select class="form-control" name="gender" id="gender">
                                	       	<option selected disabled>ជ្រើសរើស ភេទ</option>
			                      		    <option class="hidden" value="ប្រុស">ប្រុស</option>
			                      		    <option class="hidden"  value="ស្រី">ស្រី</option>
                            	  		</select>
	                           </div>
	                       </div>
	                      
	                 <div class="form-group row "  id="bordy_dob">
	                    <label class="control-label col-md-4 col-sm-3 col-xs-3 ">Date of birth :</label>
	                        <div class="col-md-8 col-sm-9 col-xs-9 input-error" >
	                            <small class="text-danger bold italic" id="dob_error"></small>
	                                <input type="date"  value ="" class="form-control" id="dob" name="dob" placeholder ="dd/mm/yyyy" autocomplete="off" >
	                           </div>
	                        </div>
                       </div>
                   </div>
                   			<center>
                   		       <button type="button" class="btn btn-next btn-primary" id="next-1">Next</button>
                   		    </center>   
                      </div>
               </fieldset>
                    
               <fieldset id ="step_two" >
                        <div class="form-top">
                             <div class="form-top-left">
                                <h3><span><i class="fa fa-calendar-check-o" aria-hidden="true"></i></span>Teacher Place Of Birth</h3>
                             </div>
                        </div>
                   <div class="form-bottom">
     
                      <div class="form-group row"  id="bordy_dob">
	                     <label class="control-label col-md-4 col-sm-3 col-xs-3">Country :</label>
	                        <div class="col-md-8 col-sm-9 col-xs-9">
	                           <small class="text-danger" id="country_error"></small>
	                               <select class="form-control" id="country" name="country" >
	                             	<option  id ="first-index" class="hidden"  selected disabled value ="0">ជ្រើសរើស ប្រទេស</option>
	                             	<option class="hidden"  value="កម្ពុជា" >កម្ពុជា</option>
	                             	<option class="hidden"  value="ថៃ" >ថៃ</option>
	                             	<option class="hidden"  value="ឡាវ" >ឡាវ</option>
	                                <option class="hidden"  value="វៀតណាម" >វៀតណាម</option>
                            	 </select>
                               <p>   </p>
	                      </div>
	                  </div>
	                       
	                    <div class="form-group row"  id="bordy_dob">
	                         <label class="control-label col-md-4 col-sm-3 col-xs-3">Province :</label>
	                             <div class="col-md-8 col-sm-9 col-xs-9">
	                                <small class="text-danger" id="province_error"></small>
	                                   <select class="form-control" id="province" name="province" >
	                             	     <option id ="province-index" class="hidden"  selected disabled value ="0" >ជ្រើសរើស ខេត្ត</option>
	                                	<%
	                            		   ArrayList<ProvinceBean >ListPro= ProvinceService.p_listAllProvince();
				                      		for(ProvinceBean pb : ListPro)
					                      	{
					                      		 out.print("<option value='"+ pb.getPro_id()+"'>" + pb.getProvince() + "</option>");
					                      	}
			                      		%>
                            	   </select>
	                          </div>
	                      </div>
                               
                               
                        <div class="form-group row"  id="bordy_dob">
	                        <label class="control-label col-md-4 col-sm-3 col-xs-3">District :</label>
	                            <div class="col-md-8 col-sm-9 col-xs-9">
	                               <small class="text-danger" id="district_error"></small>
	                                  <select class="form-control" id="district" name="district"  >
	                             	<option id ="district-index" class="hidden"  selected disabled value ="0" >ជ្រើសរើស ស្រុក</option>
                            	</select>
	                          </div>
	                     </div>
	                     
	                       
                       <div class="form-group row"  id="bordy_dob">
	                       <label class="control-label col-md-4 col-sm-3 col-xs-3">Commune:</label>
	                          <div class="col-md-8 col-sm-9 col-xs-9">
	                              <small class="text-danger" id="commune_error"></small>
	                                 <select class="form-control" id="commune" name="commune" >
	                              <option  id ="commune-index" class="hidden"  selected disabled value ="0" >ជ្រើសរើស ឃុំ</option>
                               </select>
	                        </div>
	                     </div>
	                     
                         <div class="form-group row"  id="bordy_dob">
	                        <label class="control-label col-md-4 col-sm-3 col-xs-3">Village :</label>
	                           <div class="col-md-8 col-sm-9 col-xs-9">
	                             <small class="text-danger" id="village_error"></small>
	                                <select class="form-control" id="village" name="village_id" >
	                             <option  id ="village-index" class="hidden"  selected disabled value ="0" >ជ្រើសរើស ភូមិ</option>
                            	</select>
	                        </div>
	                     </div>
							<center>
	                            <button type="button" class="btn btn-previous btn-danger" id="previous-1">Previous</button>
	                            <button type="button" class="btn btn-next btn-primary" id="next-2">Next</button>
                            </center>
                        </div>
          </fieldset>
                    
                    
          <fieldset id ="step_tree">
                       <div class="form-top">
                             <div class="form-top-left">
                                <h3><span><i class="fa fa-calendar-check-o" aria-hidden="true"></i></span>	Teacher Current Address</h3>
                             </div>
                        </div>
                   <div class="form-bottom">
     
                     
	                    <div class="form-group row"  id="bordy_dob">
	                         <label class="control-label col-md-4 col-sm-3 col-xs-3">Province :</label>
	                             <div class="col-md-8 col-sm-9 col-xs-9">
	                                <small class="text-danger" id="current_province_error"></small>
	                                   <select class="form-control" id="current_province" name="current_province" >
	                             	     <option id ="current-province-index" class="hidden"  selected disabled value="0" >Choose Current Province</option>
	                                	<%
	                            		   ArrayList<ProvinceBean >ListPr= ProvinceService.p_listAllProvince();
				                      		for(ProvinceBean pb : ListPr)
					                      	{
					                      		 out.print("<option value='"+ pb.getPro_id()+"'>" + pb.getProvince() + "</option>");
					                      	}
			                      		%>
                            	   </select>
	                          </div>
	                      </div>
                               
                               
                        <div class="form-group row"  id="bordy_dob">
	                        <label class="control-label col-md-4 col-sm-3 col-xs-3">District :</label>
	                            <div class="col-md-8 col-sm-9 col-xs-9">
	                               <small class="text-danger" id="current_district_error"></small>
	                                  <select class="form-control" id="current_district" name="current_district" >
	                             	<option  id ="current-district-index" class="hidden"  selected disabled value= "0">Choose Current District</option>
                            	</select>
	                          </div>
	                     </div>
	                     
	                       
                       <div class="form-group row"  id="bordy_dob">
	                       <label class="control-label col-md-4 col-sm-3 col-xs-3">Commune:</label>
	                          <div class="col-md-8 col-sm-9 col-xs-9">
	                              <small class="text-danger" id="current_commune_error"></small>
	                                 <select class="form-control" id="current_commune" name="current_commune" >
	                              <option class="hidden"  selected disabled value="0" >Choose Current Commune</option>
                               </select>
	                        </div>
	                     </div>
	                     
                         <div class="form-group row"  id="bordy_dob">
	                        <label class="control-label col-md-4 col-sm-3 col-xs-3">Village :</label>
	                           <div class="col-md-8 col-sm-9 col-xs-9">
	                             <small class="text-danger" id="current_village_error"></small>
	                                <select class="form-control" id="current_village" name="current_village" >
	                             <option id="current-village-index" class="hidden"  selected disabled value="0" >Choose Current Village</option>
                            	</select>
	                        </div>
	                     </div>
								<center>
								
									<button type="button" class="btn btn-previous btn-danger" id='previous-2'>Previous</button>
                                    <button type="button" class="btn btn-next btn-primary"  id="next-3" >Next</button>
								</center>
                            
                        </div> 
        </fieldset>
                    
                    
        <fieldset id="step_four" >
        
                    <div class="form-top">
                             <div class="form-top-left">
                                <h3><span><i class="fa fa-calendar-check-o" aria-hidden="true"></i></span>		Teacher RelationShip</h3>
                             </div>
                        </div>
                   <div class="form-bottom">
	                
		               <div class="form-group row">
		                     <label class="control-label col-md-4 col-sm-3 col-xs-3">Phone Number :</label>
		                         <div class="col-md-8 col-sm-9 col-xs-9">
		                           <small class="text-danger" id="phone_number_error"></small>
		                          <input type="text" class="form-control" id="phone_number" name="phone_number" placeholder ="Teacher_Phone_Number" autocomplete="off">
		                     </div>
		                </div>
		                      
		                <div class="form-group row"  id="bordy_dob">
	                         <label class="control-label col-md-4 col-sm-3 col-xs-3">Nationality :</label>
	                             <div class="col-md-8 col-sm-9 col-xs-9">
	                                 <small class="text-danger" id="nationality_error"></small>
	                                     <select class="form-control" id="nationality" name="nationality" >
	                             	        <option id ="nationality-index" class="hidden"  selected disabled value="0" >សូមជ្រើសរើស សញ្ជាតិ</option>
	                             	      	<option class="hidden"  value="ខ្មែរ">​ខ្មែរ</option>
	                             	   		<option class="hidden"  value="ថៃ">​ថៃ​​ </option>
                            	   		</select>
	                           </div>
	                     </div>
	                  
                            
		                 <div class="form-group row" id="bordy_passport_no">
		                    <label class="control-label col-md-4 col-sm-3 col-xs-3"> Nationality_Id:</label>
		                        <div class="col-md-8 col-sm-9 col-xs-9">
		                          <small class="text-danger" id="nationality_id_error"></small>
		                        <input type="text" class="form-control" id="nationality_id" name="nationality_id" placeholder ="Natoinality Id" autocomplete="off">
		                     </div>
		                 </div>
		                      
		                 <div class="form-group row" id="bordy_stutus">
		                     <label class="control-label col-md-4 col-sm-3 col-xs-3"> Passport No:</label>
		                         <div class="col-md-8 col-sm-9 col-xs-9">
		                            <small class="text-danger" id="passport_no_error"></small>
		                          <input type="text" class="form-control" id="passport_no" name="passport_no" placeholder ="Teacher_Passport_No" autocomplete="off">
		                      </div>
		                  </div>
		                      
		                  <div class="form-group row" id="bordy_stutus">
		                      <label class="control-label col-md-4 col-sm-3 col-xs-3"> Status:</label>
		                         <div class="col-md-8 col-sm-9 col-xs-9">
		                             <small class="text-danger" id="status_error"></small>
		                          	    <select class="form-control" id="status" name="status" >
		                             	  <option id ="status-index" class="hidden"  selected disabled value="0" >សូមជ្រើស រើស </option>
		                             	  <option class="hidden"  value="នូវលីវ">នូវលីវ</option>
		                             	<option class="hidden"  value="រៀបការរួច">រៀបការរួច</option>
		                             <option class="hidden"  value="ពោះម៉ាយ">ពោះម៉ាយ</option>
                            	  </select>
		                       </div>
		                    </div>    
                               
                          <div class="form-group row">
		                      <label class="control-label col-md-4 col-sm-3 col-xs-3">Photo</label>
		                         <div class="col-md-8 col-sm-9 col-xs-9">
		                            <div class="form-group">
		                                 <small class="text-danger" id="customFile_error"></small>
                                      	<img alt="register_photo" src="../../images/Avatar/register_avatar.jpg" name="photo" id="show_photo" width="130px" height="147px" style="border:1px solid gray; border-radius:2px !important;">
                                        </div>
                                	   <div class="custom-file mb-3">
                                	   
								      <input type="file" class="custom-file-input" id="customFile" name="file" onchange="readDisplayUrl(this);" value="0" >
								   	 <label class="custom-file-label" id="customFile-index" for="customFile" >Choose your photo *</label>
		                          </div>
		                       </div>
		                   </div>
		                   
                               <center>
                               		<button type="button" class="btn btn-previous btn-danger" id="previous-3">Previous</button>
                                    <button type="submit" class="btn btn-next btn-primary" id="next-4" >Next</button>
                               </center>
                        </div>
                </fieldset>
   
                <fieldset id ="step_five">
                   <div class="form-bottom">
                       <div class="row">
                           <div class="col-7">
                               <h2 class="fs-title">Finish:</h2>
                                   </div>
                                        </div> <br><br>
                                          <h2 class="purple-text text-center"><strong>SUCCESS !</strong></h2> <br>
                                         <div class="row justify-content-center">
                                     <div class="col-3" id="image-icon"> <img src="https://i.imgur.com/GwStPmg.png" class="fit-image" style="width:100px;height:100px;"> </div>
                                  </div> <br><br>
                               <div class="row justify-content-center">
                            <div class="col-7 text-center">
                      <h5 class="purple-text text-center">You Are Add New  Successfully Signed Up</h5>
                    </div>
                </div>
                     
               </div>
        </fieldset>
                    
                    
                </form>
            </div>
        </div>
    </div>      
           </div>
      
		<jsp:include page="/view/detail/FooterPage"></jsp:include>   
      </div>
    <script src="../../js/custom_js/teacher/validationTeacherAndAddTeacher.js"></script>
	<script src="../../js/custom_js/Input_Show_Image.js"></script>
    <script src="../../js/bootstrap.bundle.min.js"></script>
    <script src="../../js/custom.min.js"></script>

</body>
</html>