<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>  
<%@ page import="java.util.ArrayList" %>
<%@ page import="db.services.ProvinceService" %> 
<%@ page import="bean.ProvinceBean" %>
<%@ page import="db.services.DistrictService" %> 
<%@ page import="bean.DistrictBean" %>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="../../${param.photo_view}" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2> ${param.user_name} </h2>
              </div>
            </div>
            <!-- /menu profile quick info -->
            <br />
           <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                <%
	                String role_code = request.getParameter("role_code");
	                
	                if(role_code.equals("err")){
	               	 response.sendRedirect("../AccessSystem");
	                }
	                if(role_code.equals("role005") || role_code.equals("role001") || role_code.equals("role002")){
	                	out.print("<li><a><i class='fas fa-tachometer-alt'></i> Dashboard <span class='fa fa-chevron-down'></span></a>");
	                	out.print("<ul class='nav child_menu'>");
	                	out.print("<li><a href='#'>Dashboard</a></li>");
	                	out.print("<li><a href='#'>Dashboard2</a></li>");
	                	out.print("<li><a href='#'>Dashboard3</a></li>");
	                	out.print("</ul>");
	                	out.print("</li>");
	                }
	                if(role_code.equals("role005")){
	                	out.print("<li><a><i class='fas fa-users fa-lg'></i> Users <span class='fa fa-chevron-down'></span></a>");
	                	out.print("<ul class='nav child_menu'>");
	                	out.print("<li><a href='../../view/detail/ListUsers'>All Users</a></li>");
	                	out.print("<li><a href='#'>Dashboard2</a></li>");
	                	out.print("<li><a href='#'>Dashboard3</a></li>");
	                	out.print("</ul>");
	                	out.print("</li>");
	                }
	                if(role_code.equals("role005") || role_code.equals("role001")){
	                    out.print("<li><a><i class='fas fa-chalkboard-teacher fa-lg'> </i> Teachers <span class='fa fa-chevron-down'></span></a>");
	                    out.print("<ul class='nav child_menu'>");
	                    out.print("<li><a href='../../view/detail/ListTeacher'>ListTeacher</a></li>");
	                    out.print("<li><a href='../../view/teacher_education/ListEducationTeacher'>ListEducation</a></li>");
	                    out.print("<li><a href='../../view/teacher/NewTeacherSubject'>Teacher Course</a></li>");
	                    out.print("<li><a href='/view/teacher_education/AddEducationTeacher'>Dashboard3</a></li>");
	                    out.print("</ul>");
	                    out.print("</li>");
	                  }
	                if(role_code.equals("role005") || role_code.equals("role001")){
	                	out.print("<li><a><i class='fas fa-user-friends fa-lg'></i> Parents <span class='fa fa-chevron-down'></span></a>");
	                	out.print("<ul class='nav child_menu'>");
	                	out.print("<li><a href='../../view/list/ListGuardian'>View Parents</a></li>");
	                	out.print("<li><a href='#'>Dashboard2</a></li>");
	                	out.print("<li><a href='#'>Dashboard3</a></li>");
	                	out.print("</ul>");
	                	out.print("</li>");
	                }
	                if(role_code.equals("role005") || role_code.equals("role001") || role_code.equals("role002")){
	                	out.print("<li><a><i class='fas fa-user-graduate fa-lg'></i> Students <span class='fa fa-chevron-down'></span></a>");
	                	out.print("<ul class='nav child_menu'>");
	                	out.print("<li><a href='../../view/detail/NameList'>បញ្ជីរាយនាមសិស្ស</a></li>");
	                	out.print("<li><a href='../../view/card/StudentCard'>ប័ណ្ណសម្គាល់ខ្លួនសិស្ស</a></li>");
	                	out.print("<li><a href='../../view/detail/ListStudents'>ប្រវត្តិរួបសិស្ស</a></li>");
	                	out.print("</ul>");
	                	out.print("</li>");
	                }
	                if(role_code.equals("role005") || role_code.equals("role001")){
	                	out.print("<li><a><i class='fas fa-graduation-cap fa-lg'></i>Study Control <span class='fa fa-chevron-down'></span></a>");
	                	out.print("<ul class='nav child_menu'>");
	                	
	                	out.print("<li><a href='#'> Class Study<span class='fa fa-chevron-down'></span></a>");
        	  			out.print("<ul class='nav child_menu'>");
	  						out.print("<li class='sub_menu'><a href='../../view/list/ClassStudy'>List Class-Study</a></li>");
	  						out.print("<li class='sub_menu'><a href='../../view/StudentClass/ClassStudent'>Student Class</a></li>");
	  						out.print("<li class='sub_menu'><a href='../../view/add/SubjectClass'>Add Subject-Class</a></li>");
	  						out.print("<li class='sub_menu'><a href='../../view/add/TeacherClass'>Add Teacher-Class</a></li>");
	  						out.print("<li><a href='../../view/score/MonthlyScore'>Monthly Score</a></li>");
	  					out.print("</ul>");
	  					out.print("</li>");
	                	
	                	out.print("<li><a href='#'> Subjects<span class='fa fa-chevron-down'></span></a>");
        	  			out.print("<ul class='nav child_menu'>");
	  						out.print("<li class='sub_menu'><a href='../../view/detail/ListSubject'>Subject List</a></li>");
	  						out.print("<li><a href='../../view/detail/ListSubjectCategory'>Subject Categories</a></li>");
	  						out.print("<li><a href='#level2_2'> Level Two</a></li>");
	  					out.print("</ul>");
	  					out.print("</li>");
	  					
	  					out.print("<li><a href='#'> Classroom<span class='fa fa-chevron-down'></span></a>");
        	  			out.print("<ul class='nav child_menu'>");
	  						out.print("<li class='sub_menu'><a href='../../view/detail/classroom'>Classroom List</a></li>");
	  						out.print("<li><a href='#level2_1'> Level Two</a></li>");
	  						out.print("<li><a href='#level2_2'> Level Two</a></li>");
	  					out.print("</ul>");
	  					out.print("</li>");
	  					
	                	out.print("<li><a href='#'>Dashboard</a></li>");
	                	out.print("<li><a href='#'>Dashboard2</a></li>");
	                	out.print("<li><a href='#'>Dashboard3</a></li>");
	                	out.print("</ul>");
	                	out.print("</li>");
	                }
	                if(role_code.equals("role005") || role_code.equals("role001") || role_code.equals("role002")){
	                	out.print("<li><a><i class='fas fa-clock-o fa-lg'></i> TimeTable <span class='fa fa-chevron-down'></span></a>");
	                	out.print("<ul class='nav child_menu'>");
	                	out.print("<li><a href='#'>Teacher TimeTable</a></li>");
	                	out.print("</ul>");
	                	out.print("</li>");
	                }
	                if(role_code.equals("role005")){
	                	out.print("<li><a><i class='fas fa-layer-group fa-lg'></i> Layouts <span class='fa fa-chevron-down'></span></a>");
	                	out.print("<ul class='nav child_menu'>");
	                	out.print("<li><a href='#'>Fixed Sidebar</a></li>");
	                	out.print("<li><a href='#'>Fixed Footer</a></li>");
	                	out.print("</ul>");
	                	out.print("</li>");
	                }
                %>
                
                </ul>              
              </div>              
              <div class="menu_section">
                <h3>Live On</h3>
                <ul class="nav side-menu">
              <%
              if(role_code.equals("role005")){
            	  out.print("<li><a><i class='fas fa-address-book fa-lg'></i> Address <span class='fa fa-chevron-down'></span></a>");
            	  	out.print("<ul class='nav child_menu'>");
	            	  	out.print("<li><a href='../../view/detail/AddNewDistrict'>Add District<span class='fa fa-chevron-down'></span></a>");
	            	  	out.print("<li><a href='../../view/detail/AddNewCommune'>Add Commune<span class='fa fa-chevron-down'></span></a>");
	            	  	out.print("<li><a href='../../view/add/AddVillage'>Add Village<span class='fa fa-chevron-down'></span></a>");
		           	  	out.print("</ul>");
            	  out.print("</li>");
              }	
              if(role_code.equals("role005")){
            	  out.print("<li><a><i class='fas fa-circle fa-lg'></i> Additional Pages <span class='fa fa-chevron-down'></span></a>");
            	  	out.print("<ul class='nav child_menu'>");
            	  		out.print("<li><a href='#'> E-commerce</a></li>");
            	  		out.print("<li><a href='#'> Projects</a></li>");
            	  		out.print("<li><a href='#'> Project Detail</a></li>");
            	  		out.print("<li><a href='#'> Contacts</a></li>");
            	  		out.print("<li><a href='#'> Profile</a></li>");
            	  	out.print("</ul>");
            	  out.print("</li>");
              }
              %>                     
              </ul>
              </div>

            </div>